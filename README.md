# testbed_hw

This repository contains a number of ROS packages for the motor control of the [underWater Arm Vehicle Emulator](https://rsa-manipulation-testbed.gitlab.io/wave-documentation/), a joint project between Oregon State University and University of Washington Applied Physics Laboratory.

The main documentation for this project is at [https://rsa-manipulation-testbed.gitlab.io/wave-documentation/docs/](hhttps://rsa-manipulation-testbed.gitlab.io/wave-documentation/docs/)


## Overview

The packages in this repository are used to create a [ROS Control](http://wiki.ros.org/ros_control) `RobotHW` interface which can be controlled by the various controller algorithms offered by ROS Control.   The closely affiliated [`testbed_sw`](https://gitlab.com/rsa-manipulation-testbed/testbed_sw) repository contains URDF and configuration files for the testbed.

## Contents

This repository contains the following ROS packages:

* [`clearpath_sc_ros`](clearpath_sc_ros/) is a thin wrapper around the `SFoundation` Linux API provided by [Teknic](https://teknic.com/) for  [Clearpath-SC](https://teknic.com/products/clearpath-brushless-dc-servo-motors/clearpath-sc/)  "Software Controlled" servo motors.  This package will download and build the Clearpath API automatically.

  The core wrapper does not contain ROS-specific code, and can be compiled stand-alone using `cmake`.  The package contains a few simple command line applications to demonstrate communication and control of motors.

   This directory can also be built as a ROS package, which includes a ROS-specific abstraction [`ClearpathMotorHW`](clearpath_sc_ros/include/clearpath_sc_ros/clearpath_motor_hw.h).  It also builds a sample `RobotHw` node [`clearpath_node`](https://gitlab.com/rsa-manipulation-testbed/testbed_hw/-/blob/main/clearpath_sc_ros/nodes/clearpath_node.cpp) which controls **one motor** using ROS Control.

   See the [README](clearpath_sc_ros/README.md) for more details.

* The [`motor_hw_base`](motor_hw_base/) ROS package defines an abstract motor interface which is inherited by the [`ClearpathMotorHW`](clearpath_sc_ros/include/clearpath_sc_ros/clearpath_motor_hw.h) class in `clearpath_sc_ros` when building for ROS and used in [`testbed_driver`](testbed_driver/).

   This abstraction exists to allow use of a "simulated" motor for any axis.

* [`testbed_driver`](testbed_driver/) is the "main" testbed application.  It consists of a ROS node(let) which implements the `RobotHW` interface and coordinates all four axes of the testbed system.

    See the [README](testbed_driver/README) for more details.

* [`testbed_teleop`](testbed_teleop/) is Python ROS node which is a ROS Control **client**, providing joystick control of the testbed.

This package has many runtime dependencies from ROS Control.  It also depends on:

* [`testbed_sw`](https://gitlab.com/rsa-manipulation-testbed/testbed_sw) for URDF and configuration files,
* [`testbed_msgs`](https://gitlab.com/rsa-manipulation-testbed/testbed_msgs) for message types specific to the testbed.

we strongly suggest using `rosdep` to install all of the package dependencies.  Many of the dependencies are for runtime plugins are only resolved (or found to be missing) when trying to run the software.

# License

Each ROS package within this repo is licensed independently, although all of them are released under the [BSD 3-Clause License] by the University of Washington.   Please see the LICENSE file in each package.
