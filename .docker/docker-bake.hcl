#
# Override these variables with environment variables
# e.g.
#
#   TESTBED_ROS_DISTRO=iron docker buildx bake
#
# or
#
#   export TESTBED_ROS_DISTRO=iron
#   docker buildx bake
#
variable "TESTBED_ROS_DISTRO" { default = "noetic" }
variable "TESTBED_GITLAB_REPO" { default = "rsa-manipulation-testbed/testbed_hw" }

group "default" {
  targets = ["ci", "robot"]
}

# These are populated by the metadata-action GITLAB action for each target
# when building in CI
#
target "docker-metadata-action-ci" {}
target "docker-metadata-action-robot" {}

#
# All images can pull cache from the images published at GITLAB
# or local storage (within the Buildkit image)
#
# ... and push cache to local storage
#
target "ci" {
  inherits = ["docker-metadata-action-ci"]
  dockerfile = ".docker/Dockerfile"
  target = "ci"
  context = ".."
  args = {
    ROS_DISTRO = "${TESTBED_ROS_DISTRO}"
  }
  tags = [
    "registry.gitlab.com/${TESTBED_GITLAB_REPO}:${TESTBED_ROS_DISTRO}-ci"
  ]
  labels = {
    "org.opencontainers.image.source" = "https://gitlab.com/${TESTBED_GITLAB_REPO}"
  }
  cache_from =[
    "registry.gitlab.com/${TESTBED_GITLAB_REPO}:cache-${TESTBED_ROS_DISTRO}-ci",
    "registry.gitlab.com/${TESTBED_GITLAB_REPO}:cache-${TESTBED_ROS_DISTRO}-robot",
    "type=local,dest=.docker-cache"
  ]
  cache_to = [
    "type=local,dest=.docker-cache"
  ]
}

target "robot" {
  inherits = [ "ci", "docker-metadata-action-robot" ]
  target = "robot"
  tags = [
    "registry.gitlab.com/${TESTBED_GITLAB_REPO}:${TESTBED_ROS_DISTRO}-robot"
  ]
  cache_to = [
    "type=local,dest=.docker-cache"
  ]
}
