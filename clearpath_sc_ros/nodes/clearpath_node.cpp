// A minimalist RobotHW for a single Clearpath motor.
// For development and testing, not for production use
// on testbed.
//
// Copyright 2021 University of Washington
//

#include <ros/ros.h>

#include "clearpath_robot_hw.h"

using clearpath_sc_ros::ClearpathRobotHW;

int main(int argc, char **argv) {
  ros::init(argc, argv, "clearpath_node");
  ros::NodeHandle nh("clearpath");
  ros::NodeHandle pnh("~");

  ClearpathRobotHW clearpath;

  if (!clearpath.init(nh, pnh)) {
    ROS_FATAL("clearpath.init() failed");
    return -1;
  }

  ros::AsyncSpinner spinner(2);
  spinner.start();

  ros::waitForShutdown();

  return 0;
}
