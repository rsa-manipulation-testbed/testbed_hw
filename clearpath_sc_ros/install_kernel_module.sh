#!/usr/bin/env bash

URL="https://s3.us-west-1.wasabisys.com/marburg/Clearpath_Linux_Software_2021-04-12.tar.gz"
URL_HASH_SHA1=5a9d51b5327958a94a6d2c6aa9a94f628fa3b68c
KERNEL_TARBALL="Teknic_SC4Hub_USB_Driver.tar"
MODULE_NAME="xr_usb_serial_common"

set -e

function moduleExist(){
  MODULE="$1"
  if lsmod | grep "$MODULE" &> /dev/null ; then
    return 0
  else
    return 1
  fi
}

echo "Checking if module already exists..."
if moduleExist $MODULE_NAME; then
    echo "Module \"$MODULE_NAME\" installed, don't need to do anything"
    exit 0
fi

echo "... Module not currently loaded; attempting to load module ..."
sudo modprobe $MODULE_NAME || MODPROBE=$?

if [[ -z "$MODPROBE" ]]; then
    if moduleExist $MODULE_NAME; then
        echo "Successfully loaded \"$MODULE_NAME\", don't need to do anything"
        exit 0
    fi
fi


echo "... Module does not exist, building from source"

TMPDIR=`mktemp -d`
echo "Working in temporary dir $TMPDIR"
cd $TMPDIR

sudo apt install -y linux-headers-`uname -r` gcc wget

wget -O clearpath.tar.gz  $URL
echo "$URL_HASH_SHA1 clearpath.tar.gz" > sha1sum.txt
sha1sum --check sha1sum.txt

tar -xzvf clearpath.tar.gz

if [[ ! -f $KERNEL_TARBALL ]]; then
    echo "Expected file \"$KERNEL_TARBALL\" not found.  Hmmm"
    exit -1
fi

tar -xvf $KERNEL_TARBALL && cd ExarKernelDriver/

## Must patch the driver if 5.15 or later
CURRENT_VERSION=$(uname -r | cut -d"." -f1,2)
if (( $(echo "$CURRENT_VERSION >= 5.15" | bc -l) )); then
    echo "Linux Kernel 5.15 (or greater), applying patch..."
    wget https://gitlab.com/rsa-manipulation-testbed/testbed_hw/-/raw/main/clearpath_sc_ros/xr_usb_serial_common_kernel_5.15.0.patch
    patch -p1 < xr_usb_serial_common_kernel_5.15.0.patch
fi

sudo ./Install_DRV_SCRIPT.sh

if ! moduleExist $MODULE_NAME; then
    echo "After building the module, it still isn't loaded.   Hmmm"
    exit -1
fi
