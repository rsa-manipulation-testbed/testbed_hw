# clearpath_sc_ros

This package is a thin convenience wrapper around the `SFoundation` Linux API provided by [Teknic](https://teknic.com/) for  [Clearpath-SC](https://teknic.com/products/clearpath-brushless-dc-servo-motors/clearpath-sc/)  "Software Controlled" servo motors.  This package will download and build the Clearpath API automatically.

The core library does not contain ROS-specific code, and can be compiled stand-alone using `cmake`, including a few simple command line applications for testing motors.

This directory can also be built as a ROS package, in which case it builds a sample `RobotHw` node for controlling on motor using ROS Control.

# Building (non-ROS)

This package follows the standard CMake build procedure:

```
mkdir build
cd build
cmake ..
make
```

This will compile the command line tools/example found in the [tools/](tools/) directory including:

* [`enumerate_nodes`](tools/enumerate_nodes.cpp) which is based on an example from the Clearpath API.  It will list the nodes (motors) attached to this computer.  This program **does not** enable or move the motors.

* [`clearpath_client`](tools/clearpath_client.cpp) connects to a single motor and allows control of motor position.  Note that this program **WILL** enable and move one of the motors.

# Building (ROS)

This package can be built as part of a Catkin workspace.

It builds a ROS node, `clearpath_node` which implements a ROS Control `RobotHW` for a single Clearpath motor, specified by serial number.  The Launchfile [`clearpath.launch`](launch/clearpath.launch) shows a sample launchfile for the node.

The node [`clearpath_teleop`](nodes_py/clearpath_teleop) is a Python node which allows control of the `clearpath_node` via joystick.  It can be launched via the [`teleop.launch`](launch/teleop.launch) launchfile.

Generally, neither of these nodes would be used in production.   `clearpath_node` provides a sample template for a more sophisticated `RobotHW` which would be adapted to the particulars of a given hardware configuration.
