
#include <iostream>

using std::cerr;
using std::cout;
using std::endl;

#include "clearpath_sc_ros/manager.h"

using namespace clearpath_sc_ros;

int main(int argc, char **argv) {
  try {
    Manager mgr;
    mgr.initialize();

    std::cout << "Found " << mgr.axes().size() << " nodes on " << mgr.numPorts()
              << " ports" << std::endl;

    for (auto const &axis : mgr.axes()) {
      IInfo &info(axis->getInfo());

      std::cout << "  " << info.Model.Value() << " : s/n "
                << info.SerialNumber.Value() << " : f/w "
                << info.FirmwareVersion.Value() << std::endl;

      auto const pos = axis->position();

      std::cout << "   Current position: " << pos.raw() << " counts; "
                << pos.metric() << " rad" << std::endl;
    }

  } catch (mnErr &theErr) {
    std::cerr << "Caught error" << std::endl;
    printf("Caught error: addr=%d, err=0x%08x\nmsg=%s\n", theErr.TheAddr,
           theErr.ErrorCode, theErr.ErrorMsg);

    return -1;
  }

  return 0;
}
