
#include <unistd.h>

#include <iostream>

using std::cerr;
using std::cout;
using std::endl;

#include "clearpath_sc_ros/manager.h"

using namespace clearpath_sc_ros;

int main(int argc, char **argv) {
  try {
    Manager mgr;
    mgr.initialize();

    std::cout << "Found " << mgr.axes().size() << " nodes on " << mgr.numPorts()
              << " ports" << std::endl;

    for (auto const &axis : mgr.axes()) {
      // axis->enable();
      IInfo &info(axis->getInfo());
      std::cout << "  " << info.Model.Value() << " : s/n "
                << info.SerialNumber.Value() << " : f/w "
                << info.FirmwareVersion.Value() << std::endl;

      std::cout << " ++     Initial velocity limit " << axis->velocityLimit()
                << " rad/s" << std::endl;
      std::cout << " ++ Initial acceleration limit "
                << axis->accelerationLimit() << " rad/s^2" << std::endl;
      std::cout << " ++       Initial torque limit " << axis->maxTorquePct()
                << "%" << std::endl;
      std::cout << " ++        Initial max current " << axis->maxCurrent()
                << " A" << std::endl;

      std::cout << " ++         Initial jerk limit " << axis->jerkLimit()
                << std::endl;
      std::cout << " ++   Initial jerk limit delay " << axis->jerkLimitDelay()
                << std::endl;

      std::cout << " ++           Network watchdog "
                << axis->networkWatchdogMs() << " ms" << std::endl;

      std::cout << "++ Checking for errors..." << std::endl;
      axis->checkErrors();

      std::cout << "============================================" << std::endl;
    }

  } catch (mnErr &theErr) {
    std::cerr << "Caught error" << std::endl;
    printf("Caught error: addr=%d, err=0x%08x\nmsg=%s\n", theErr.TheAddr,
           theErr.ErrorCode, theErr.ErrorMsg);

    return -1;
  }

  return 0;
}
