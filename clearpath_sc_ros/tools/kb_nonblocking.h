// Copyright 2021 University of Washington
//
// Released under the BSD license, see LICENSE in this repo

#include <sys/select.h>
#include <termios.h>

#pragma once

void nonblock(bool enable) {
  struct termios ttystate;

  // get the terminal state
  tcgetattr(STDIN_FILENO, &ttystate);

  if (enable) {
    // turn off canonical mode
    ttystate.c_lflag &= ~ICANON;
    // minimum of number input read.
    ttystate.c_cc[VMIN] = 1;
  } else {
    // turn on canonical mode
    ttystate.c_lflag |= ICANON;
  }

  // set the terminal attributes.
  tcsetattr(STDIN_FILENO, TCSANOW, &ttystate);
}

unsigned char kbhit() {
  struct timeval tv;
  fd_set fds;
  tv.tv_sec = 0;
  tv.tv_usec = 0;
  FD_ZERO(&fds);
  FD_SET(STDIN_FILENO, &fds);
  select(STDIN_FILENO + 1, &fds, NULL, NULL, &tv);
  return FD_ISSET(STDIN_FILENO, &fds);
}
