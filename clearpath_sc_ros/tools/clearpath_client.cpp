// Copyright 2021 University of Washington
//
// Released under the BSD license, see LICENSE in this repo
//
// A simple command line client for running a TMotor in position mode

#include <boost/program_options.hpp>
#include <chrono>
#include <cmath>
#include <csignal>
#include <functional>
#include <iostream>
namespace po = boost::program_options;

#include "clearpath_sc_ros/manager.h"
#include "kb_nonblocking.h"

using namespace clearpath_sc_ros;
using namespace sFnd;

namespace {
// the signal_handler can't take a lambda directly,
// but it can call the global lambda "signal_function",
// which we can set elsewhere
std::function<void(int)> signal_function;
void signal_handler(int signal) { signal_function(signal); }
}  // anonymous namespace

class ClearpathClient {
 public:
  ClearpathClient() : _done(false) {
    signal(SIGINT, signal_handler);
    signal_function = [&](int signum) {
      if (signum == SIGINT) {
        _done = true;
        return;
      }

      std::cout << "Exiting with signal " << signum << std::endl;
      exit(signum);
    };
  }

  ~ClearpathClient() { nonblock(false); }

  void dumpNodes() {
    try {
      _mgr.initialize();
      std::cout << "Found " << _mgr.axes().size() << " nodes on "
                << _mgr.numPorts() << " ports" << std::endl;

      std::shared_ptr<ClearpathMotor> theAxis;
      for (auto &axis : _mgr.axes()) {
        IInfo &info(axis->getInfo());

        std::cout << "  " << info.Model.Value() << " : s/n "
                  << info.SerialNumber.Value() << " : f/w "
                  << info.FirmwareVersion.Value() << std::endl;
      }
    } catch (mnErr &theErr) {
      printf("Caught error: addr=%d, err=0x%08x\nmsg=%s\n", theErr.TheAddr,
             theErr.ErrorCode, theErr.ErrorMsg);
    }
  }

  void run(int sn) {
    try {
      _mgr.initialize();
      std::cout << "Found " << _mgr.axes().size() << " nodes on "
                << _mgr.numPorts() << " ports" << std::endl;

      std::cout << "Disabling global shutdown" << std::endl;
      _mgr.setGlobalShutdown(false);

      std::shared_ptr<ClearpathMotor> theAxis;
      for (auto &axis : _mgr.axes()) {
        IInfo &info(axis->getInfo());

        std::cout << "      " << info.Model.Value() << " : s/n "
                  << info.SerialNumber.Value() << " : f/w "
                  << info.FirmwareVersion.Value() << std::endl;

        if (int(info.SerialNumber.Value()) == sn) {
          std::cout << "   Found motor s/n " << sn << std::endl;
          theAxis = axis;
        }
      }

      if (!theAxis) {
        std::cerr << "Unable to find a motor with serial number " << sn
                  << std::endl;
        return;
      }

      {
        IInfo &info(theAxis->getInfo());
        std::cout << "Using " << info.Model.Value() << " : s/n "
                  << info.SerialNumber.Value() << " : f/w "
                  << info.FirmwareVersion.Value() << std::endl;
      }

      auto const networkms = theAxis->networkWatchdogMs();
      std::cout << "Network watchdog " << networkms << std::endl;
      theAxis->setNetworkWatchdogMs(10000);
      std::cout << "Network watchdog " << theAxis->networkWatchdogMs()
                << std::endl;

      std::cout << "Enabling ..." << std::endl;
      theAxis->enable();

      auto const startTime = std::chrono::steady_clock::now();

      double pos = theAxis->position().metric();

      const float delta = 0.1;  // radians

      unsigned int count = 0;
      while (!_done) {
        bool has_alert = theAxis->checkErrors();

        if (has_alert) {
          std::cout << "!!! Clearing alerts" << std::endl;

          theAxis->clearErrors();
        }

        if ((count % 10) == 0) {
          std::chrono::duration<float> dt =
              (std::chrono::steady_clock::now() - startTime);

          std::cout << "[" << dt.count()
                    << "]  Pos = " << theAxis->position(false).metric()
                    << " : Vel = " << theAxis->velocity().metric()
                    << " : Current = " << theAxis->current() << std::endl;
        }

        if (kbhit() != 0) {
          auto ch = getchar();
          std::cout << "Got: " << ch << std::endl;
          if ((ch > 0) && (isprint(ch))) {
            if (toupper(ch) == '<') {
              pos -= delta;
              theAxis->positionMove(pos, true);
              std::cout << "NEW COMMANDED POSITION: "
                        << theAxis->positionCmd().metric() << std::endl;
            } else if (toupper(ch) == '>') {
              pos += delta;
              theAxis->positionMove(pos, true);
              std::cout << "NEW COMMANDED POSITION: "
                        << theAxis->positionCmd().metric() << std::endl;

            } else if (toupper(ch) == 'Q') {
              _done = true;
            }
          }
        }

        usleep(100000);
      }

      std::cout << " ... disabling" << std::endl;
      theAxis->disable();

      theAxis->setNetworkWatchdogMs(networkms);

    } catch (mnErr &theErr) {
      printf("Caught error: addr=%d, err=0x%08x\nmsg=%s\n", theErr.TheAddr,
             theErr.ErrorCode, theErr.ErrorMsg);
    }
  }

 private:
  Manager _mgr;

  bool _done;
};

int main(int argc, char **argv) {
  nonblock(true);

  try {
    // Parse command line options
    // clang-format off
    po::options_description options("Options");
    options.add_options()
        ("help", "produce help message")
        ("sn", po::value<int>()->default_value(-1));
    // clang-format on

    po::variables_map vm;
    po::store(parse_command_line(argc, argv, options), vm);
    po::notify(vm);

    if (vm.count("help")) {
      std::cout << options << std::endl;
      exit(0);
    }

    // Not sure how to motors are specified on the command line
    if (vm["sn"].as<int>() == -1) {
      std::cerr
          << "Specify serial number of motor on command line with --sn option"
          << std::endl;
      std::cerr << "Detected motors:" << std::endl;

      ClearpathClient client;
      client.dumpNodes();
      exit(0);
    }

    int motorSn = vm["sn"].as<int>();

    ClearpathClient client;
    client.run(motorSn);

  } catch (const po::error &ex) {
    std::cerr << ex.what() << '\n';
    exit(-1);
  } catch (mnErr &theErr) {
    printf("Caught error: addr=%d, err=0x%08x\nmsg=%s\n", theErr.TheAddr,
           theErr.ErrorCode, theErr.ErrorMsg);

    exit(-1);
  }

  exit(0);
}
