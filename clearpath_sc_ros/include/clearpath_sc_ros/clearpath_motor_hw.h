//
// ROS-specific wrapper for a single instance of ClearpathMotor(),
// which represents one Clearpath SC motor
//
// Copyright 2021 University of Washington
//
#pragma once

#include <diagnostic_updater/publisher.h>
#include <dynamic_reconfigure/server.h>
#include <ros/node_handle.h>
#include <ros/publisher.h>
#include <ros/timer.h>

#include <atomic>
#include <condition_variable>
#include <functional>
#include <memory>
#include <string>

#include "clearpath_sc_ros/ClearpathMotorParamsConfig.h"
#include "clearpath_sc_ros/clearpath_motor.h"
#include "motor_hw_base/motor_hw_base.h"

namespace clearpath_sc_ros {

class ClearpathMotorHw : public testbed_driver::MotorHwBase {
 public:
  typedef std::shared_ptr<ClearpathMotorHw> Ptr;

  static const float DiagnosticUpdateTimerPeriod;

  ClearpathMotorHw(const std::string &name, const ros::NodeHandle &parentnh,
                   const ClearpathMotor::Ptr &axis);

  virtual ~ClearpathMotorHw();

  bool enable() override;
  bool disable() override;

  bool isEnabled() override;

  void setPosition(double p) override;
  void setVelocity(double v, double max_allowed_dt = -0.0,
                   bool scale_if_exceeds_dt = false) override;

  void stop(bool abrupt = false) override;

  virtual testbed_driver::MotorState state() const override;

 protected:
  void dynamicReconfigureCallback(
      clearpath_sc_ros::ClearpathMotorParamsConfig &config, uint32_t level);
  typedef dynamic_reconfigure::Server<
      clearpath_sc_ros::ClearpathMotorParamsConfig>
      DynamicReconfigureServer;
  std::shared_ptr<DynamicReconfigureServer> _dynamicReconfigureServer;

  // -- Diagnostics objects --
  diagnostic_updater::Updater _diagnosticUpdater;
  ros::Timer _diagnosticUpdateTimer;
  std::shared_ptr<diagnostic_updater::TopicDiagnostic> _stateFreqUpdater;

  std::string stateAsString();

  ClearpathMotor::Ptr _axis;

  ros::Publisher _clearpathPublisher;

  std::mutex _blockMutex;
  std::condition_variable _blockCv;
};

}  // namespace clearpath_sc_ros
