//
// C++ class for a single Clearpath Motor
// This is a "medium-weight" wrapper around the
// COM-style abstractions offered by the SFoundation
// with some added functionality.
//
// Base on the "ClearpathMotor.h" from the Clearpath SDK examples
//
// Copyright 2021 University of Washington
//

#pragma once

#include <time.h>

#include <memory>

#include "Clearpath/inc-pub/pubSysCls.h"
#include "motor_hw_base/encoder_value.h"

namespace clearpath_sc_ros {

using namespace sFnd;
using namespace std;

using testbed_driver::EncoderValue;

#define RAS_CODE 3
#define TRQ_PCT 10
#define ACC_LIM_RPM_PER_SEC 800
#define VEL_LIM_RPM 200
#define TRACKING_LIM (_node.Info.PositioningResolution.Value() / 4)

//******************************************************************************
//
// DESCRIPTION
//	Provides an OO interface to one Clearpath motor.
//  Largely a thin, opinionated wrapper around calls to the Clearpath API
//
//  Unless otherwise noted:
//   * all positions in radians
//   * all velocities in rad/sec
//   * all currents in amps
//
class ClearpathMotor {
 public:
  typedef std::shared_ptr<ClearpathMotor> Ptr;

  ClearpathMotor(INode &node);
  ~ClearpathMotor();

  // Retrieve an info struct
  sFnd::IInfo &getInfo() { return _node.Info; }
  int32_t serialNumber() const { return _serial_number; }

  bool enable();

  // "enabled" is the specific case of "has the user enabled to motor"?
  // e.g. a motor can be enabled and yet also be e-stopped
  bool isEnabled();

  // "ready" is the broader case of "will the motor move"?
  bool isReady();

  bool disable();

  // Set velocity limit in rad/sec
  void setVelocityLimit(double newVelLimit);
  double velocityLimit();

  // Set acceleration limit in rad/sec^2
  void setAccelerationLimit(double newAccLimit);
  double accelerationLimit();

  // Set max torque in Amps
  void setMaxCurrent(double newTorqueLimit);
  double maxCurrent();

  // Get set max torque as a pct (0-100) of max torque
  bool setMaxTorquePct(double newTorqueLimitPct);
  double maxTorquePct();

  // Get/set jerk limit (units?) and jerk limit delay
  void setJerkLimit(unsigned int newJerkLimit);
  unsigned int jerkLimit();

  void setJerkLimitDelay(double newJerkLimitDelay);
  double jerkLimitDelay();

  // Return motor state (in radians, rad/sec, amps)
  EncoderValue position(bool includeWraps = true);
  EncoderValue velocity();
  double current();

  // Return commanded position / velocity / torque
  EncoderValue positionCmd(bool includeWraps = true);
  EncoderValue velocityCmd();
  double currentCmd();

  // Initiate a move
  bool positionMove(double targetPosn, bool isAbsolute = false,
                    bool addDwell = false);

  // Commands the motor to move to the target_vel (given in rad/sec)
  //
  // If max_allowed_dt > 0, the API will calculate a predicted time
  // to achieve the target vel.  If that time is greater than max_allowed_dt
  // (in seconds),
  //
  //    if scale_if_exceeds_dt is false, no velocity is sent to the motor
  //       and the function returns false
  //
  //    if scale_if_exceeds_dt is true, target_vel is scaled to an
  //       achievable value and sent to the motor
  bool velocityMove(double target_vel, double max_allowed_dt = -1.0,
                    bool scale_if_exceeds_dt = false);

  void stop(bool abrupt = false);

  // A blunt hammer of a function right now
  bool checkErrors(bool try_clear = false);
  void clearErrors(void);

  // This is effectively "reset zero so the current position is <offset>"
  void setPosition(double offset = 0.0);

  // Starts a homing procedure --- homing behaviors **must** be
  // pre-programmed into the motor using the Windows GUI
  void initiateHoming();

  double countsToRad(double c) const { return c * _radPerCount; }

  void setNetworkWatchdogMs(double val);
  double networkWatchdogMs();

 private:
  INode &_node;  // The ClearPath-SC for this ClearpathMotor

  int32_t _serial_number;

  uint32_t _resolution;    // Counts per rotation
  uint32_t _countsPerRad;  // Counts per radians
  float _radPerCount;      // Radians per count

  // Filename for the stored config file
  // std::string _configFile;

  // struct DefaultLimits _default_limits;

  int32_t m_positionWrapCount;

  void init();
};
//																			   *
//******************************************************************************

}  // namespace clearpath_sc_ros
