// Copyright 2021 University of Washington
//
// ClearpathMotor is a thin non-ROS abstraction around a single Clearpath-SC
// motor. It is largely an attempt to OOP the non-OOP Clearpath API It is based
// on the sample "ClearpathMotor.cpp" from the Clearpath SDK example
//

#include "clearpath_sc_ros/clearpath_motor.h"

#include <math.h>
#include <stdio.h>

#include <iostream>

namespace clearpath_sc_ros {

ClearpathMotor::ClearpathMotor(INode &node)
    : _node(node), m_positionWrapCount(0) {
  init();
};

ClearpathMotor::~ClearpathMotor() { disable(); }

void ClearpathMotor::init() {
  // Always use counts and current internally in the motor
  _node.AccUnit(INode::COUNTS_PER_SEC2);
  _node.VelUnit(INode::COUNTS_PER_SEC);
  _node.TrqUnit(INode::AMPS);

  _resolution = _node.Info.PositioningResolution.Value();

  // Precalculated convenience constants
  _countsPerRad = _resolution / (2 * M_PI);
  _radPerCount = 2 * M_PI / _resolution;

  // std::cout << "Resolution: " << _resolution << "  count / rev" << std::endl;
  // std::cout << "Counts per radian: " << _countsPerRad << " count / rad"
  //           << std::endl;
  // std::cout << "Radians per count: " << _radPerCount << " rad / count"
  //           << std::endl;
}

void ClearpathMotor::setVelocityLimit(double newVelLimit) {
  try {
    _node.Motion.VelLimit = newVelLimit * _countsPerRad;
  } catch (mnErr &theErr) {
    printf("Caught error: addr=%d, err=0x%08x\nmsg=%s\n", theErr.TheAddr,
           theErr.ErrorCode, theErr.ErrorMsg);
  }
}

double ClearpathMotor::velocityLimit() {
  _node.Motion.VelLimit.Refresh();
  return _node.Motion.VelLimit.Value() * _radPerCount;
}

void ClearpathMotor::setAccelerationLimit(double newAccelLimit) {
  // Convert rad/sec^2 to counts/sec^2
  try {
    _node.Motion.AccLimit = newAccelLimit * _countsPerRad;
  } catch (mnErr &theErr) {
    printf("Caught error: addr=%d, err=0x%08x\nmsg=%s\n", theErr.TheAddr,
           theErr.ErrorCode, theErr.ErrorMsg);
  }
}

double ClearpathMotor::accelerationLimit() {
  _node.Motion.AccLimit.Refresh();
  return _node.Motion.AccLimit.Value() * _radPerCount;
}

void ClearpathMotor::setMaxCurrent(double newTorqueLimit) {
  try {
    _node.Limits.TrqGlobal = newTorqueLimit;
  } catch (mnErr &theErr) {
    printf("Caught error: addr=%d, err=0x%08x\nmsg=%s\n", theErr.TheAddr,
           theErr.ErrorCode, theErr.ErrorMsg);
  }
}

double ClearpathMotor::maxCurrent() {
  _node.Limits.TrqGlobal.Refresh();
  return _node.Limits.TrqGlobal.Value();
}

bool ClearpathMotor::setMaxTorquePct(double newTorqueLimitPct) {
  if ((newTorqueLimitPct < 0.0) || (newTorqueLimitPct > 100.0)) return false;

  try {
    _node.TrqUnit(_node.PCT_MAX);
    _node.Limits.TrqGlobal = newTorqueLimitPct;
    _node.TrqUnit(_node.AMPS);
  } catch (mnErr &theErr) {
    printf("Caught error: addr=%d, err=0x%08x\nmsg=%s\n", theErr.TheAddr,
           theErr.ErrorCode, theErr.ErrorMsg);
    return false;
  }
  return true;
}

double ClearpathMotor::maxTorquePct() {
  _node.TrqUnit(_node.PCT_MAX);
  _node.Limits.TrqGlobal.Refresh();
  auto trq = _node.Limits.TrqGlobal.Value();
  _node.TrqUnit(_node.AMPS);

  return trq;
}

void ClearpathMotor::setJerkLimit(unsigned int newJerkLimit) {
  try {
    _node.Motion.JrkLimit = newJerkLimit;
  } catch (mnErr &theErr) {
    printf("Caught error: addr=%d, err=0x%08x\nmsg=%s\n", theErr.TheAddr,
           theErr.ErrorCode, theErr.ErrorMsg);
  }
}

unsigned int ClearpathMotor::jerkLimit() {
  _node.Motion.JrkLimit.Refresh();
  return _node.Motion.JrkLimit.Value();
}

void ClearpathMotor::setJerkLimitDelay(double newJerkLimitDelay) {
  try {
    _node.Motion.JrkLimitDelay = newJerkLimitDelay;
  } catch (mnErr &theErr) {
    printf(
        "Caught error while setting jerk limit delay: addr=%d, "
        "err=0x%08x\nmsg=%s\n",
        theErr.TheAddr, theErr.ErrorCode, theErr.ErrorMsg);
  }
}

double ClearpathMotor::jerkLimitDelay() {
  _node.Motion.JrkLimitDelay.Refresh();
  return _node.Motion.JrkLimitDelay.Value();
}

double ClearpathMotor::networkWatchdogMs() {
  _node.Setup.Ex.NetWatchdogMsec.Refresh();
  return _node.Setup.Ex.NetWatchdogMsec.Value();
}

//=============

bool ClearpathMotor::enable() {
  try {
    // Clear alerts and node stops
    _node.Status.AlertsClear();
    _node.Motion.NodeStopClear();

    std::cerr << "--> Enabling Node <--" << std::endl;
    _node.EnableReq(true);

    // If the node is not currently ready, wait for it to get there
    time_t timeout;
    timeout = time(NULL) + 5;

    while (!_node.Status.IsReady()) {
      if (time(NULL) > timeout) {
        std::cout << "Error: Timed out waiting for enable" << std::endl;
        return false;
      }
    }
  } catch (mnErr &theErr) {
    printf("Caught error: addr=%d, err=0x%08x\nmsg=%s\n", theErr.TheAddr,
           theErr.ErrorCode, theErr.ErrorMsg);
    return false;
  }

  return true;
}

bool ClearpathMotor::disable() {
  std::cout << "--> Disabling Node <--" << std::endl;

  _node.EnableReq(false);

  // Poll the status register to confirm the node's disable
  time_t timeout;
  timeout = time(NULL) + 3;
  _node.Status.RT.Refresh();
  while (_node.Status.RT.Value().cpm.Enabled) {
    if (time(NULL) > timeout) {
      std::cerr << "Error: Timed out waiting for disable" << std::endl;
      return false;
    }
    _node.Status.RT.Refresh();
  }

  return true;
}

bool ClearpathMotor::isEnabled() { return _node.EnableReq(); }

bool ClearpathMotor::isReady() { return _node.Motion.IsReady(); }

bool ClearpathMotor::positionMove(double targetRad, bool isAbsolute,
                                  bool addDwell) {
  if (!isReady()) {
    std::cout << "Motor is not ready, not moving" << std::endl;
    return false;
  }

  // Check for number-space wrapping. With successive moves, the position may
  // pass the 32-bit number cap (+ or -). The check below looks for that and
  // updates our counter to be used with GetPosition
  // if (!isAbsolute) {
  //   if (position(false) + targetPosn > INT32_MAX) {
  //     m_positionWrapCount++;
  //   } else if (position(false) + targetPosn < INT32_MIN) {
  //     m_positionWrapCount--;
  //   }
  // }

  int64_t count = targetRad * _countsPerRad;

  try {
    // std::cout << " Commanding position " << count << " counts" << std::endl;

    _node.Motion.MovePosnStart(count, isAbsolute, addDwell);
  } catch (mnErr &theErr) {
    printf("Caught error: addr=%d, err=0x%08x\nmsg=%s\n", theErr.TheAddr,
           theErr.ErrorCode, theErr.ErrorMsg);

    if (theErr.ErrorCode == MN_ERR_CMD_MV_FULL) {
      std::cout << "Move buffer full ... " << std::endl;
    }

    return false;
  }

  return true;
}

//
//
//
bool ClearpathMotor::velocityMove(double target_vel, double max_allowed_dt,
                                  bool scale_if_exceeds_dt) {
  if (!isReady()) {
    std::cerr << "Motor is not ready, not moving" << std::endl;
    return false;
  }

  int64_t cps = target_vel * _countsPerRad;

  _node.Motion.VelMeasured.Refresh();
  int64 current_vel = _node.Motion.VelMeasured.Value();
  _node.Motion.AccLimit.Refresh();

  try {
    std::cout << " Commanding velocity " << cps << " counts/sec" << std::endl;

    if (max_allowed_dt > 0.0) {
      const double max_allowed_dt_ms = max_allowed_dt * 1000.0;

      // double time_to_vel = _node.Motion.MoveVelDurationMsec(cps-current_vel);

      double time_to_vel =
          (cps - current_vel) * 1000.0 / _node.Motion.AccLimit.Value();
      if (time_to_vel > max_allowed_dt_ms) {
        std::cerr << "Requested velocity " << cps << " from " << current_vel
                  << " would take " << time_to_vel << " ms with accel limit "
                  << _node.Motion.AccLimit.Value()
                  << ", greater than allowed interval of " << max_allowed_dt_ms
                  << " ms" << std::endl;

        if (scale_if_exceeds_dt) {
          const double VEL_FUDGE = 2.0;
          const double vel_scale = VEL_FUDGE * max_allowed_dt_ms / time_to_vel;

          // This simple linear scale is probably not accurate, but expedient
          cps = std::min(static_cast<int64_t>(vel_scale * cps), cps);
          std::cerr << "  .. scaled velocity request to "
                    << cps * 1.0 / _countsPerRad << " rad/s " << cps
                    << " counts/sec)" << std::endl;
        } else {
          // Do not move
          return false;
        }
      }
    }

    _node.Motion.MoveVelStart(cps);

  } catch (mnErr &theErr) {
    printf("Caught error: addr=%d, err=0x%08x\nmsg=%s\n", theErr.TheAddr,
           theErr.ErrorCode, theErr.ErrorMsg);

    if (theErr.ErrorCode == MN_ERR_CMD_MV_FULL) {
      std::cout << "Move buffer full" << std::endl;
      return false;
    }

    return true;
  }

  return true;
}

void ClearpathMotor::stop(bool abrupt) {
  if (abrupt) {
    std::cerr << "** Node stop abrupt!" << std::endl;
    _node.Motion.NodeStop(STOP_TYPE_ABRUPT);
  } else {
    std::cerr << "** Node stop ramp!" << std::endl;
    _node.Motion.NodeStop(STOP_TYPE_RAMP);
  }
}

EncoderValue ClearpathMotor::position(bool includeWraps) {
  _node.Motion.PosnMeasured.Refresh();

  int64_t position_counts = int64_t(_node.Motion.PosnMeasured.Value());

  if (includeWraps) {
    position_counts += int64_t(m_positionWrapCount) << 32;
  }

  return EncoderValue(position_counts, _radPerCount);
}

EncoderValue ClearpathMotor::velocity() {
  _node.Motion.VelMeasured.Refresh();
  return EncoderValue(int64_t(_node.Motion.VelMeasured.Value()), _radPerCount);
}

double ClearpathMotor::current() {
  _node.Motion.TrqMeasured.Refresh();
  return double(_node.Motion.TrqMeasured.Value());
}

EncoderValue ClearpathMotor::positionCmd(bool includeWraps) {
  _node.Motion.PosnCommanded.Refresh();

  int64_t position_counts = int64_t(_node.Motion.PosnCommanded.Value());

  if (includeWraps) {
    position_counts += int64_t(m_positionWrapCount) << 32;
  }

  return EncoderValue(position_counts, _radPerCount);
}

EncoderValue ClearpathMotor::velocityCmd() {
  _node.Motion.VelCommanded.Refresh();

  return EncoderValue(int64_t(_node.Motion.VelCommanded.Value()), _radPerCount);
}

double ClearpathMotor::currentCmd() {
  _node.Motion.TrqCommanded.Refresh();
  return int64_t(_node.Motion.TrqCommanded.Value());
}

bool ClearpathMotor::checkErrors(bool try_clear) {
  char alertList[256];
  bool has_errors = false;

  _node.Status.RT.Refresh();
  _node.Status.Alerts.Refresh();

  // Check to see if the node experienced torque saturation
  if (_node.Status.HadTorqueSaturation()) {
    std::cout
        << "      Node has experienced torque saturation since last checking"
        << std::endl;
    has_errors = true;
  }

  // get an alert register reference, check the alert register directly for
  // alerts
  if (_node.Status.Alerts.Value().isInAlert()) {
    // get a copy of the alert register bits and a text description of all bits
    // set
    _node.Status.Alerts.Value().StateStr(alertList, 256);
    std::cout << "   Node has alerts! Alerts:" << alertList << std::endl;

    // can access specific alerts using the method below
    if (_node.Status.Alerts.Value().cpm.Common.EStopped) {
      std::cout << "      Node is e-stopped" << std::endl;
      has_errors = true;
      _node.Motion.NodeStopClear();
    }
    if (_node.Status.Alerts.Value().cpm.TrackingShutdown) {
      std::cout << "      Node exceeded Tracking error limit" << std::endl;
      has_errors = true;
    }

    if (try_clear) {
      clearErrors();
    }
  }
  return has_errors;
}

void ClearpathMotor::clearErrors() {
  char alertList[256];

  std::cout << "      Clearing non-serious alerts" << std::endl;
  _node.Status.AlertsClear();
  _node.Motion.NodeStopClear();

  // Are there still alerts?
  _node.Status.Alerts.Refresh();

  if (_node.Status.Alerts.Value().isInAlert()) {
    _node.Status.Alerts.Value().StateStr(alertList, 256);
    std::cout << "   Node has serious, non-clearable alerts: " << alertList
              << std::endl;
  } else {
    std::cout << "   Node " << _node.Info.Ex.Addr()
              << ": all alerts have been cleared" << std::endl;
  }
}

void ClearpathMotor::setPosition(double offset) {
  _node.Motion.AddToPosition(-_node.Motion.PosnMeasured.Value() + offset);
}

void ClearpathMotor::initiateHoming(void) {
  std::cerr << "This function doesn't do anything" << std::endl;
}

}  // namespace clearpath_sc_ros
