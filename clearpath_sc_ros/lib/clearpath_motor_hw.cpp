// Copyright 2021 University of Washington

#include <chrono>
#include <string>
using namespace std::chrono_literals;

#include "clearpath_sc_ros/clearpath_motor_hw.h"
#include "testbed_msgs/ClearpathState.h"

namespace clearpath_sc_ros {

using testbed_driver::MotorHwBase;
using testbed_driver::MotorState;

const float ClearpathMotorHw::DiagnosticUpdateTimerPeriod = 0.1;

ClearpathMotorHw::ClearpathMotorHw(const std::string &name,
                                   const ros::NodeHandle &parentnh,
                                   const ClearpathMotor::Ptr &axis)
    : MotorHwBase(name, parentnh), _axis(axis) {
  ros::NodeHandle nh(parentnh, name);
  ros::NodeHandle pnh(std::string("~") + name);

  ROS_INFO_STREAM(" ++     Initial velocity limit " << _axis->velocityLimit()
                                                    << " rad/s");
  ROS_INFO_STREAM(" ++ Initial acceleration limit "
                  << _axis->accelerationLimit() << " rad/s^2");
  ROS_INFO_STREAM(" ++       Initial torque limit " << _axis->maxTorquePct()
                                                    << "%");
  ROS_INFO_STREAM(" ++        Initial max current " << _axis->maxCurrent()
                                                    << " A");

  ROS_INFO_STREAM(" ++         Initial jerk limit " << _axis->jerkLimit());
  ROS_INFO_STREAM(" ++   Initial jerk limit delay " << _axis->jerkLimitDelay());

  //
  _dynamicReconfigureServer = std::make_shared<DynamicReconfigureServer>(pnh);
  _dynamicReconfigureServer->setCallback(
      std::bind(&ClearpathMotorHw::dynamicReconfigureCallback, this,
                std::placeholders::_1, std::placeholders::_2));

  // -- Set up the diagnostic updater --
  _diagnosticUpdater.setHardwareID(_name);
  _diagnosticUpdater.add(
      "state", [&](diagnostic_updater::DiagnosticStatusWrapper &stat) {
        stat.add("state", stateAsString());
      });
  _diagnosticUpdateTimer = nh.createTimer(
      ros::Duration(ClearpathMotorHw::DiagnosticUpdateTimerPeriod),
      [&](const ros::TimerEvent &event) { _diagnosticUpdater.update(); });

  _clearpathPublisher =
      nh.advertise<testbed_msgs::ClearpathState>("clearpath_state", 10);

  double min_freq = 0.5;
  double max_freq = 2;
  _stateFreqUpdater = std::make_shared<diagnostic_updater::TopicDiagnostic>(
      "state", _diagnosticUpdater,
      diagnostic_updater::FrequencyStatusParam(&min_freq, &max_freq, 0.1, 10),
      diagnostic_updater::TimeStampStatusParam());
}

ClearpathMotorHw::~ClearpathMotorHw() {
  ROS_DEBUG_STREAM("Disabling " << _name);
  disable();
}

void ClearpathMotorHw::dynamicReconfigureCallback(
    clearpath_sc_ros::ClearpathMotorParamsConfig &config, uint32_t level) {
  if (config.vel_limit >= 0) {
    ROS_INFO_STREAM("  ... Setting velocity limit to " << config.vel_limit);
    _axis->setVelocityLimit(config.vel_limit);
  }

  if (config.accel_limit >= 0) {
    ROS_INFO_STREAM("  ... Setting acceleration limit to "
                    << config.accel_limit);
    _axis->setAccelerationLimit(config.accel_limit);
  }

  if (config.torque_limit >= 0) {
    auto torque_limit_pct = config.torque_limit * 100;
    ROS_INFO_STREAM("  ... Setting torque_limit to " << torque_limit_pct);
    _axis->setMaxTorquePct(torque_limit_pct);
  }

  // if (config.jerk_limit >= 0) {
  //   ROS_INFO_STREAM("  ... Setting jerk limit to " << config.jerk_limit);
  //   _axis->setJerkLimit(config.jerk_limit);
  // }

  ROS_INFO_STREAM(" ++     New velocity limit " << _axis->velocityLimit()
                                                << " rad/s");
  ROS_INFO_STREAM(" ++ New acceleration limit " << _axis->accelerationLimit()
                                                << " rad/s^2");
  ROS_INFO_STREAM(" ++       New torque limit " << _axis->maxTorquePct()
                                                << "%");
  ROS_INFO_STREAM(" ++        New max current " << _axis->maxCurrent() << " A");

  ROS_INFO_STREAM(" ++         New jerk limit " << _axis->jerkLimit());
  ROS_INFO_STREAM(" ++   New jerk limit delay " << _axis->jerkLimitDelay());
}

bool ClearpathMotorHw::enable() { return _axis->enable(); }

bool ClearpathMotorHw::disable() { return _axis->disable(); }

bool ClearpathMotorHw::isEnabled() { return _axis->isEnabled(); }

void ClearpathMotorHw::setPosition(double p) { _axis->positionMove(p); }

void ClearpathMotorHw::setVelocity(double v, double max_allowed_dt,
                                   bool scale_if_exceeds_dt) {
  _axis->velocityMove(v, max_allowed_dt, scale_if_exceeds_dt);
}

void ClearpathMotorHw::stop(bool abrupt) { _axis->stop(abrupt); }

testbed_driver::MotorState ClearpathMotorHw::state() const {
  // ~~ Query for error states ~~

  // make sure our registers are up to date
  _axis->checkErrors(true);  //\todo{}  Shouldn't blanket cancel any error, some
                             // are OK to clear (low bus voltage)

  testbed_driver::MotorState state;
  state.position = _axis->position();
  state.velocity = _axis->velocity();
  state.current = _axis->current();

  {
    testbed_msgs::ClearpathState cp_msg;
    cp_msg.header.stamp = ros::Time::now();
    cp_msg.serial_number = _axis->serialNumber();
    cp_msg.position = state.position.trueRaw();
    cp_msg.position_resolution = state.position.rawToMetric();
    cp_msg.velocity = state.velocity.trueRaw();
    cp_msg.velocity_resolution = state.velocity.rawToMetric();
    cp_msg.current = state.current.trueRaw();
    cp_msg.enabled = _axis->isEnabled();
    _clearpathPublisher.publish(cp_msg);
  }

  return state;
}

std::string ClearpathMotorHw::stateAsString() {
  if (_axis->isEnabled()) {
    return std::string("enabled");
  } else {
    return std::string("disabled");
  }
}

}  // namespace clearpath_sc_ros
