from setuptools import setup
from catkin_pkg.python_setup import generate_distutils_setup

d = generate_distutils_setup(
    packages=["testbed_teleop"],
    scripts=["nodes/testbed_teleop"],
    package_dir={"testbed_teleop": "lib/testbed_teleop"},
)

setup(**d)
