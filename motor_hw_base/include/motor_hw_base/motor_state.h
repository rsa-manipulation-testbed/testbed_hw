#pragma once

#include "motor_hw_base/encoder_value.h"

namespace testbed_driver {

struct MotorState {
  EncoderValue position, velocity, current;
};

}  // namespace testbed_driver
