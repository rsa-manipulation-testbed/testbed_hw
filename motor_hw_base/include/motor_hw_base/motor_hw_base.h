// Copyright 2021 University of Washington
//
// Abstract wrapper for a single motor
//

#pragma once

#include <ros/node_handle.h>
#include <ros/service_server.h>

#include <string>

#include "motor_hw_base/motor_state.h"
#include "testbed_msgs/AxisEnable.h"
#include "testbed_msgs/SetAxisEncoder.h"

namespace testbed_driver {

class MotorHwBase {
 public:
  typedef std::shared_ptr<MotorHwBase> SharedPtr;

  explicit MotorHwBase(const std::string &name,
                       const ros::NodeHandle &parentnh);

  virtual ~MotorHwBase();

  virtual bool enable() = 0;
  virtual bool disable() = 0;

  virtual bool isEnabled() = 0;

  virtual void setPosition(double p) = 0;
  virtual void setVelocity(double v, double max_allowed_dt = 0.0,
                           bool scale_if_exceeds_dt = false) = 0;

  virtual void stop(bool abrupt = false) { setVelocity(0.0); }

  virtual MotorState state() const = 0;

 protected:
  std::string _name;

  // -- Node runs its a variety of its own srv/msgs for testing and debug --
  bool axisEnableSrvCallback(testbed_msgs::AxisEnableRequest &,
                             testbed_msgs::AxisEnableResponse &);
  ros::ServiceServer _axisEnableServer;
};

}  // namespace testbed_driver
