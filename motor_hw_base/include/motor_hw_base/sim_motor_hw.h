// Copyright 2021 University of Washington
//
// ROS driver for one TMotor, wraps the hardware interface
// in libcubemars_can
//
#pragma once

#include <string>

#include "motor_hw_base/motor_hw_base.h"

namespace testbed_driver {

class SimMotorHw : public MotorHwBase {
 public:
  static const float ControlLoopPeriod;

  explicit SimMotorHw(const std::string &name, const ros::NodeHandle &parentnh);

  enum SimMode_t { SIM_DISABLED, SIM_IDLE, SIM_POSITION, SIM_VELOCITY };

  virtual ~SimMotorHw();

  bool enable() override {
    if (mode_ == SIM_DISABLED) mode_ = SIM_IDLE;

    std::cout << "Enabling sim motor " << _name << std::endl;

    return true;
  }

  bool disable() override {
    velocity_ = 0.0;
    mode_ = SIM_DISABLED;
    std::cout << "Disabling sim motor " << _name << std::endl;
    return true;
  }

  bool isEnabled() { return mode_ != SIM_DISABLED; }

  void setPosition(double p) override {
    if (mode_ != SIM_DISABLED) {
      mode_ = SIM_POSITION;
      cmd_velocity_ = velocity_ = 0.0;
      cmd_position_ = position_ = p;
    }
  }

  void setVelocity(double v, double max_allowed_dt = 0.0,
                   bool scale_if_exceeds_dt = false) override {
    if (mode_ != SIM_DISABLED) {
      mode_ = SIM_VELOCITY;
      cmd_velocity_ = velocity_ = v;
    }
  }

  MotorState state() const override;

 protected:
  std::string modeToString(const SimMode_t mode) const;

  SimMode_t mode_;

  float cmd_velocity_, cmd_position_;
  float velocity_, position_;

  void update(const ros::TimerEvent &e);
  ros::Timer _controlLoopTimer;
};

}  // namespace testbed_driver
