// Copyright 2021 University of Washington
//
// CoreXY platform implemented as a ros_control transmission
//
// Based on
// [differential_transmission.h](https://github.com/ros-controls/ros_control/blob/noetic-devel/transmission_interface/include/transmission_interface/differential_transmission.h)
// See the original license in include/testbed_driver/core_xy_tranmission.h

#include "testbed_driver/core_xy_transmission.h"

#include <ros/console.h>

#include <iostream>

namespace testbed_driver {

using namespace transmission_interface;

CoreXYTransmission::CoreXYTransmission(
    const std::vector<double> &actuator_reduction)
    : act_reduction_(actuator_reduction) {
  if (numActuators() != act_reduction_.size()) {
    throw TransmissionInterfaceException(
        "Reduction vector of a differential transmission must have size 2.");
  }

  if (0.0 == act_reduction_[0] || 0.0 == act_reduction_[1]) {
    throw TransmissionInterfaceException(
        "Transmission reduction ratios cannot be zero.");
  }
}

void CoreXYTransmission::actuatorToJointEffort(const ActuatorData &act_data,
                                               JointData &jnt_data) {
  assert(numActuators() == act_data.effort.size() &&
         numJoints() == jnt_data.effort.size());

  assert(act_data.effort[0] && act_data.effort[1] && jnt_data.effort[0] &&
         jnt_data.effort[1]);

  // \todo  Not sure about effort conversions
  //   const std::vector<double>& ar = act_reduction_;

  //   *jnt_data.effort[0] = jr[0] * (*act_data.effort[0] * ar[0] +
  //   *act_data.effort[1] * ar[1]); *jnt_data.effort[1] = jr[1] *
  //   (*act_data.effort[0] * ar[0] - *act_data.effort[1] * ar[1]);
}

void CoreXYTransmission::actuatorToJointVelocity(const ActuatorData &act_data,
                                                 JointData &jnt_data) {
  assert(numActuators() == act_data.velocity.size() &&
         numJoints() == jnt_data.velocity.size());
  assert(act_data.velocity[0] && act_data.velocity[1] && jnt_data.velocity[0] &&
         jnt_data.velocity[1]);

  const std::vector<double> &ar = act_reduction_;

  *jnt_data.velocity[0] =
      (*act_data.velocity[0] / ar[0] + *act_data.velocity[1] / ar[1]) / (2.0);

  *jnt_data.velocity[1] =
      (*act_data.velocity[0] / ar[0] - *act_data.velocity[1] / ar[1]) / (2.0);
}

void CoreXYTransmission::actuatorToJointPosition(const ActuatorData &act_data,
                                                 JointData &jnt_data) {
  assert(numActuators() == act_data.position.size() &&
         numJoints() == jnt_data.position.size());
  assert(act_data.position[0] && act_data.position[1] && jnt_data.position[0] &&
         jnt_data.position[1]);

  const std::vector<double> &ar = act_reduction_;

  *jnt_data.position[0] =
      (*act_data.position[0] / ar[0] + *act_data.position[1] / ar[1]) / (2.0);

  *jnt_data.position[1] =
      (*act_data.position[0] / ar[0] - *act_data.position[1] / ar[1]) / (2.0);
}

void CoreXYTransmission::actuatorToJointAbsolutePosition(
    const ActuatorData &act_data, JointData &jnt_data) {
  assert(numActuators() == act_data.absolute_position.size() &&
         numJoints() == jnt_data.absolute_position.size());
  assert(act_data.position[0] && act_data.absolute_position[1] &&
         jnt_data.absolute_position[0] && jnt_data.absolute_position[1]);

  const std::vector<double> &ar = act_reduction_;

  *jnt_data.absolute_position[0] = (*act_data.absolute_position[0] / ar[0] +
                                    *act_data.absolute_position[1] / ar[1]) /
                                   (2.0);
  *jnt_data.absolute_position[1] = (*act_data.absolute_position[0] / ar[0] -
                                    *act_data.absolute_position[1] / ar[1]) /
                                   (2.0);
}

void CoreXYTransmission::jointToActuatorEffort(const JointData &jnt_data,
                                               ActuatorData &act_data) {
  assert(numActuators() == act_data.effort.size() &&
         numJoints() == jnt_data.effort.size());
  assert(act_data.effort[0] && act_data.effort[1] && jnt_data.effort[0] &&
         jnt_data.effort[1]);

  const std::vector<double> &ar = act_reduction_;

  *act_data.effort[0] =
      (*jnt_data.effort[0] + *jnt_data.effort[1]) / (2.0 * ar[0]);
  *act_data.effort[1] =
      (*jnt_data.effort[0] - *jnt_data.effort[1]) / (2.0 * ar[1]);
}

void CoreXYTransmission::jointToActuatorVelocity(const JointData &jnt_data,
                                                 ActuatorData &act_data) {
  //   ROS_WARN_STREAM( "   CoreXY joint: " << *jnt_data.velocity[0] << ", " <<
  //   *jnt_data.velocity[1]); ROS_WARN_STREAM( "CoreXY actuator: " <<
  //   *act_data.velocity[0] << ", " << *act_data.velocity[1]);

  assert(numActuators() == act_data.velocity.size() &&
         numJoints() == jnt_data.velocity.size());
  assert(act_data.velocity[0] && act_data.velocity[1] && jnt_data.velocity[0] &&
         jnt_data.velocity[1]);

  const std::vector<double> &ar = act_reduction_;

  *act_data.velocity[0] =
      (*jnt_data.velocity[0] + *jnt_data.velocity[1]) * ar[0];
  *act_data.velocity[1] =
      (*jnt_data.velocity[0] - *jnt_data.velocity[1]) * ar[1];
}

void CoreXYTransmission::jointToActuatorPosition(const JointData &jnt_data,
                                                 ActuatorData &act_data) {
  assert(numActuators() == act_data.position.size() &&
         numJoints() == jnt_data.position.size());
  assert(act_data.position[0] && act_data.position[1] && jnt_data.position[0] &&
         jnt_data.position[1]);

  const std::vector<double> &ar = act_reduction_;

  *act_data.position[0] =
      (*jnt_data.position[0] + *jnt_data.position[1]) * ar[0];
  *act_data.position[1] =
      (*jnt_data.position[0] - *jnt_data.position[1]) * ar[1];
}

}  // namespace testbed_driver
