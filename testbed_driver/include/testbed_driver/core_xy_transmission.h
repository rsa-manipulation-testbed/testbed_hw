// CoreXY platform implemented as a ros_control transmission
//
// Based on
// [differential_transmission.h](https://github.com/ros-controls/ros_control/blob/noetic-devel/transmission_interface/include/transmission_interface/differential_transmission.h)
//
// Retaining the license of the original file below...
///////////////////////////////////////////////////////////////////////////////
// Copyright (C) 2013, PAL Robotics S.L.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//   * Redistributions of source code must retain the above copyright notice,
//     this list of conditions and the following disclaimer.
//   * Redistributions in binary form must reproduce the above copyright
//     notice, this list of conditions and the following disclaimer in the
//     documentation and/or other materials provided with the distribution.
//   * Neither the name of PAL Robotics S.L. nor the names of its
//     contributors may be used to endorse or promote products derived from
//     this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//////////////////////////////////////////////////////////////////////////////

#pragma once

#include <transmission_interface/transmission.h>
#include <transmission_interface/transmission_interface_exception.h>

#include <cassert>
#include <string>
#include <vector>

namespace testbed_driver {

/**
 * \brief Implementation of a CoreXY mechanism
 *
 * CoreXY relates two actuators (A and B) to two axes of motion (joints) (X and
 * Y)
 *
 * From https://corexy.com/theory.html
 *
 *  dX = (dA + dB)/2
 *  dY = (dA - dB)/2
 *
 *  dA = dX + dY
 *  dB = dX - dY
 *
 * Where dA, dB, dX, dY are all linear motions in the same units.
 *
 * To convert rotational motion to {dA,dB}, then multiplying by the actuator
 * ratio converts linear motion to actuator motion e.g. if the actuator position
 * is measured in radian, then the actuator ratio is 1/radius of the actuator
 * pulley (in the same units as the belt motion)
 *
 *
 * <CENTER>
 * <table>
 * <tr><th></th><th><CENTER>Effort</CENTER></th><th><CENTER>Velocity</CENTER></th><th><CENTER>Position</CENTER></th></tr>
 * <tr><td>
 * <b> Actuator to joint </b>
 * </td>
 * <td>
 * \f{eqnarray*}{
 * \tau_{j_1} & = & n_{j_1} ( n_{a_1} \tau_{a_1} + n_{a_2} \tau_{a_2} )
 * \\[2.5em] \tau_{j_2} & = & n_{j_2} ( n_{a_1} \tau_{a_1} + n_{a_2} \tau_{a_2}
 * ) \f}
 * </td>
 * <td>
 * \f{eqnarray*}{
 * \dot{x}_{j_1} & = & \frac{ \dot{x}_{a_1} / n_{a_1} + \dot{x}_{a_2} / n_{a_2}
 * }{2 n_{j_1}} \\[1em] \dot{x}_{j_2} & = & \frac{ \dot{x}_{a_1} / n_{a_1} -
 * \dot{x}_{a_2} / n_{a_2} }{2 n_{j_2}} \f}
 * </td>
 * <td>
 * \f{eqnarray*}{
 * x_{j_1} & = & \frac{ x_{a_1} / n_{a_1} + x_{a_2} / n_{a_2} }{2 n_{j_1}}  +
 * x_{off_1} \\[1em] x_{j_2} & = & \frac{ x_{a_1} / n_{a_1} - x_{a_2} / n_{a_2}
 * }{2 n_{j_1}}  + x_{off_2} \f}
 * </td>
 * </tr>
 * <tr><td>
 * <b> Joint to actuator </b>
 * </td>
 * <td>
 * \f{eqnarray*}{
 * \tau_{a_1} & = & \frac{ \tau_{j_1} / n_{j_1} + \tau_{j_2} / n_{j_2} }{2
 * n_{a_1}} \\[1em] \tau_{a_2} & = & \frac{ \tau_{j_1} / n_{j_1} - \tau_{j_2} /
 * n_{j_2} }{2 n_{a_1}} \f}
 * </td>
 * <td>
 * \f{eqnarray*}{
 * \dot{x}_{a_1} & = & n_{a_1} ( n_{j_1} \dot{x}_{j_1} + n_{j_2} \dot{x}_{j_2} )
 * \\[2.5em] \dot{x}_{a_2} & = & n_{a_2} ( n_{j_1} \dot{x}_{j_1} - n_{j_2}
 * \dot{x}_{j_2} ) \f}
 * </td>
 * <td>
 * \f{eqnarray*}{
 * x_{a_1} & = & n_{a_1} \left[ n_{j_1} (x_{j_1} - x_{off_1}) + n_{j_2} (x_{j_2}
 * - x_{off_2}) \right] \\[2.5em] x_{a_2} & = & n_{a_2} \left[ n_{j_1} (x_{j_1}
 * - x_{off_1}) - n_{j_2} (x_{j_2} - x_{off_2}) \right] \f}
 * </td></tr></table>
 * </CENTER>
 *
 * where:
 * - \f$ x \f$, \f$ \dot{x} \f$ and \f$ \tau \f$ are position, velocity and
 * effort variables, respectively.
 * - Subindices \f$ _a \f$ and \f$ _j \f$ are used to represent actuator-space
 * and joint-space variables, respectively.
 * - \f$ x_{off}\f$ represents the offset between motor and joint zeros,
 * expressed in joint position coordinates (cf. SimpleTransmission class
 * documentation for a more detailed description of this variable).
 * - \f$ n \f$ represents a transmission ratio. Reducers/amplifiers are allowed
 * on both the actuator and joint sides (depicted as timing belts in the
 * figure). A transmission ratio can take any real value \e except zero. In
 * particular:
 *     - If its absolute value is greater than one, it's a velocity reducer /
 * effort amplifier, while if its absolute value lies in \f$ (0, 1) \f$ it's a
 * velocity amplifier / effort reducer.
 *     - Negative values represent a direction flip, ie. input and output move
 * in opposite directions.
 *     - <b>Important:</b> Use transmission ratio signs to match this class'
 * convention of positive actuator/joint directions with a given mechanical
 * design, as they will in general not match.
 *
 * \ingroup transmission_types
 */
class CoreXYTransmission : public transmission_interface::Transmission {
 public:
  /**
   * \param actuator_reduction    Reduction ratio of actuators.
   * \pre Nonzero actuator and joint reduction values.
   */
  CoreXYTransmission(const std::vector<double> &actuator_reduction);

  /**
   * \brief Transform \e effort variables from actuator to joint space.
   * \param[in]  act_data Actuator-space variables.
   * \param[out] jnt_data Joint-space variables.
   * \pre Actuator and joint effort vectors must have size 2 and point to valid
   * data. To call this method it is not required that all other data vectors
   * contain valid data, and can even remain empty.
   */
  void actuatorToJointEffort(
      const transmission_interface::ActuatorData &act_data,
      transmission_interface::JointData &jnt_data) override;

  /**
   * \brief Transform \e velocity variables from actuator to joint space.
   * \param[in]  act_data Actuator-space variables.
   * \param[out] jnt_data Joint-space variables.
   * \pre Actuator and joint velocity vectors must have size 2 and point to
   * valid data. To call this method it is not required that all other data
   * vectors contain valid data, and can even remain empty.
   */
  void actuatorToJointVelocity(
      const transmission_interface::ActuatorData &act_data,
      transmission_interface::JointData &jnt_data) override;

  /**
   * \brief Transform \e position variables from actuator to joint space.
   * \param[in]  act_data Actuator-space variables.
   * \param[out] jnt_data Joint-space variables.
   * \pre Actuator and joint position vectors must have size 2 and point to
   * valid data. To call this method it is not required that all other data
   * vectors contain valid data, and can even remain empty.
   */
  void actuatorToJointPosition(
      const transmission_interface::ActuatorData &act_data,
      transmission_interface::JointData &jnt_data) override;

  /**
   * \brief Transform \e absolute encoder values from actuator to joint space.
   * \param[in]  act_data Actuator-space variables.
   * \param[out] jnt_data Joint-space variables.
   * \pre Actuator, joint position and absolute encoder position vectors must
   * have the same size. To call this method it is not required that all other
   * data vectors contain valid data, and can even remain empty.
   */
  void actuatorToJointAbsolutePosition(
      const transmission_interface::ActuatorData &act_data,
      transmission_interface::JointData &jnt_data) override;

  /**
   * \brief Transform \e torque sensor values from actuator to joint space.
   * \param[in]  act_data Actuator-space variables.
   * \param[out] jnt_data Joint-space variables.
   * \pre Actuator, joint position and torque sensor vectors must have the same
   * size. To call this method it is not required that all other data vectors
   * contain valid data, and can even remain empty.
   */
  // void actuatorToJointTorqueSensor(const
  // transmission_interface::ActuatorData& act_data,
  //                                        transmission_interface::JointData&
  //                                        jnt_data) override;

  /**
   * \brief Transform \e effort variables from joint to actuator space.
   * \param[in]  jnt_data Joint-space variables.
   * \param[out] act_data Actuator-space variables.
   * \pre Actuator and joint effort vectors must have size 2 and point to valid
   * data. To call this method it is not required that all other data vectors
   * contain valid data, and can even remain empty.
   */
  void jointToActuatorEffort(
      const transmission_interface::JointData &jnt_data,
      transmission_interface::ActuatorData &act_data) override;

  /**
   * \brief Transform \e velocity variables from joint to actuator space.
   * \param[in]  jnt_data Joint-space variables.
   * \param[out] act_data Actuator-space variables.
   * \pre Actuator and joint velocity vectors must have size 2 and point to
   * valid data. To call this method it is not required that all other data
   * vectors contain valid data, and can even remain empty.
   */
  void jointToActuatorVelocity(
      const transmission_interface::JointData &jnt_data,
      transmission_interface::ActuatorData &act_data) override;

  /**
   * \brief Transform \e position variables from joint to actuator space.
   * \param[in]  jnt_data Joint-space variables.
   * \param[out] act_data Actuator-space variables.
   * \pre Actuator and joint position vectors must have size 2 and point to
   * valid data. To call this method it is not required that all other data
   * vectors contain valid data, and can even remain empty.
   */
  void jointToActuatorPosition(
      const transmission_interface::JointData &jnt_data,
      transmission_interface::ActuatorData &act_data) override;

  std::size_t numActuators() const override { return 2; }
  std::size_t numJoints() const override { return 2; }

  bool hasActuatorToJointAbsolutePosition() const override { return true; }
  bool hasActuatorToJointTorqueSensor() const override { return false; }

  void setActuatorReduction(double r) { act_reduction_ = {r, r}; }

  const std::vector<double> &getActuatorReduction() const {
    return act_reduction_;
  }

 protected:
  std::vector<double> act_reduction_;
};

}  // namespace testbed_driver
