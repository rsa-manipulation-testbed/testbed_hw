// Copyright 2021 University of Washington

#pragma once

#include <controller_manager/controller_manager.h>
#include <diagnostic_updater/publisher.h>
#include <dynamic_reconfigure/server.h>
#include <hardware_interface/controller_info.h>
#include <hardware_interface/joint_command_interface.h>
#include <hardware_interface/joint_state_interface.h>
#include <hardware_interface/robot_hw.h>
#include <joint_limits_interface/joint_limits.h>
#include <joint_limits_interface/joint_limits_interface.h>
#include <transmission_interface/simple_transmission.h>
#include <transmission_interface/transmission.h>
#include <transmission_interface/transmission_interface.h>

#include <array>
#include <string>

#include "clearpath_sc_ros/manager.h"
#include "motor_hw_base/motor_hw_base.h"
#include "testbed_driver/core_xy_transmission.h"

// msgs and srvs
#include "testbed_msgs/AxisEnable.h"
#include "testbed_msgs/SetAxisEncoder.h"

namespace testbed_driver {

// Testbed is a "wrapper" around the "full system"
class Testbed : public hardware_interface::RobotHW {
 public:
  static const float DiagnosticUpdateTimerPeriod;

  static const float DefaultCoreXyPulleyRadius;
  static const float DefaultZAxisScrewTravel;
  static const float DefaultYawGearRatio;

  enum Joint_t { CoreXyX = 0, CoreXyY = 1, ZAxis = 2, Yaw = 3, NumJoints = 4 };

  enum Actuator_t {
    CoreXyA = 0,
    CoreXyB = 1,
    ZMotor = 2,
    YawMotor = 3,
    NumActuators = 4
  };

  enum JointControlMode { Control_Idle = 0, Control_Velocity = 1 };

  static const std::array<std::string, NumJoints> JointNames;
  static const std::array<std::string, NumActuators> ActuatorNames;

  Testbed();
  ~Testbed();

  virtual bool init(ros::NodeHandle &, ros::NodeHandle &);

  virtual void read(const ros::Time &, const ros::Duration &);
  virtual void write(const ros::Time &, const ros::Duration &);

  // Called when controllers switch
  virtual void doSwitch(
      const std::list<hardware_interface::ControllerInfo> & /*start_list*/,
      const std::list<hardware_interface::ControllerInfo> & /*stop_list*/);

 private:
  // Service call to enable/disable some or all of the axes
  bool enableSrvCallback(testbed_msgs::AxisEnableRequest &,
                         testbed_msgs::AxisEnableResponse &);
  ros::ServiceServer enable_all_server_;

  clearpath_sc_ros::Manager manager_;
  MotorHwBase::SharedPtr _yaw, _coreXyA, _coreXyB, _zAxis;

  JointControlMode yaw_control_mode_, coreXY_control_mode_, zaxis_control_mode_;

  ros::Timer diagnostic_update_timer_;
  diagnostic_updater::Updater diagnostic_updater_;

  ros::Publisher testbed_state_pub_, joint_command_input_pub_,
      joint_command_limited_pub_, actuator_command_pub_;

  // Notation:
  //
  //   A "Joint" is the high-level abstraction of the system
  //     state:  X,Y,Z, and yaw
  //
  //   An "Actuator" is the low-level motor -- yaw, z, and the two
  //     coreXY T-motors (called A and B)
  //
  //   All axes use a Transmission:
  //     * CoreXY uses the CoreXYTransmission
  //     * Yaw uses a SimpleTransmission for its gear ratio
  //     * ZAxis uses a SimpleTransmission for the gear and lead screw ratios
  //

  // Current joint states. All states are stored as a 4-tuple {X,Y,Z,yaw}
  std::array<double, NumJoints> joint_position_;
  std::array<double, NumJoints> joint_velocity_;
  std::array<double, NumJoints> joint_current_;

  // Commands to the joints, stored as a 4-tuple {X,Y,Z,yaw}
  std::array<double, NumJoints> joint_velocity_command_;

  // States for the **actuators**
  std::array<double, NumActuators> actuator_position_;
  std::array<double, NumActuators> actuator_velocity_;
  std::array<double, NumActuators> actuator_current_;
  std::array<double, NumActuators> actuator_velocity_command_;

  std::array<int64_t, NumActuators> actuator_offset_raw_;

  // Interface for publishing joint state out
  hardware_interface::JointStateInterface joint_state_interface_;

  // Interfaces for joint position and velocity commands in
  hardware_interface::VelocityJointInterface joint_velocity_interface_;

  // Interface for enforcing limits on a velocity-controlled joint through
  // saturation
  joint_limits_interface::JointLimits _limits;
  joint_limits_interface::VelocityJointSaturationInterface
      velocity_joint_limit_interface_;

  //==== CoreXY transmission ====
  // Transmission interfaces
  transmission_interface::ActuatorToJointStateInterface
      act_to_joint_state_;  // For propagating actuator state to joint
                            // space
  transmission_interface::JointToActuatorVelocityInterface
      joint_to_act_vel_;  // For propagating joint velocity commands to actuator
                          // velocity commands

  // The CoreXY transmission
  CoreXYTransmission coreXY_trans_;
  transmission_interface::ActuatorData coreXY_trans_act_state_;
  transmission_interface::ActuatorData coreXY_trans_act_cmd_;
  transmission_interface::JointData coreXY_trans_joint_state_;
  transmission_interface::JointData coreXY_trans_joint_cmd_;

  // The yaw transmission
  transmission_interface::SimpleTransmission yaw_trans_;
  transmission_interface::ActuatorData yaw_trans_act_state_;
  transmission_interface::ActuatorData yaw_trans_act_cmd_;
  transmission_interface::JointData yaw_trans_joint_state_;
  transmission_interface::JointData yaw_trans_joint_cmd_;

  // The Z-axis lead-screw transmission
  transmission_interface::SimpleTransmission zaxis_trans_;
  transmission_interface::ActuatorData zaxis_trans_act_state_;
  transmission_interface::ActuatorData zaxis_trans_act_cmd_;
  transmission_interface::JointData zaxis_trans_joint_state_;
  transmission_interface::JointData zaxis_trans_joint_cmd_;

  // Control loop
  void update(const ros::TimerEvent &e);
  ros::Timer control_loop_timer_;
  ros::Duration elapsed_time_;
  static const double ControlLoopTimerPeriod;

  boost::shared_ptr<controller_manager::ControllerManager> controller_manager_;
};

}  // namespace testbed_driver
