#include <ros/ros.h>

#include "testbed_driver/testbed.h"

using testbed_driver::Testbed;

int main(int argc, char **argv) {
  ros::init(argc, argv, "testbed_node");
  ros::NodeHandle nh;
  ros::NodeHandle pnh("~");

  Testbed testbed;

  if (!testbed.init(nh, pnh)) {
    ROS_FATAL("testbed.init() failed");
    return -1;
  }

  ros::AsyncSpinner spinner(2);
  spinner.start();

  ros::waitForShutdown();

  return 0;
}
